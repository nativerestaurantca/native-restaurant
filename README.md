Native Restaurant is Chef Nyesha Arrington's latest progressive expression of Love through food where she finds inspiration in the Santa Monica farmer’s market for her New American bistro.

Address: 620 Santa Monica Blvd, Suite A, Santa Monica, CA 90401, USA
Phone: 310-458-4427
